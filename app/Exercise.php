<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exercise extends Model
{
    protected $fillable = ['title', 'difficultyLevel', 'minutesToComplete', 'ownerId'];

    public $timestamps = false;
}
