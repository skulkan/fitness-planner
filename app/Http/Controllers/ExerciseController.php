<?php

namespace App\Http\Controllers;

use App\Exercise;
use App\Http\Middleware\GetUserFromHeader;
use Illuminate\Http\Request;

class ExerciseController extends Controller
{
    public function index()
    {
        return Exercise::all();
    }

    public function show($id)
    {
        return Exercise::find($id);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        if (!$this->isValidInput($data)) {
            return Response()->json(['error' => 'Property minutesToComplete shouldn\'t be bigger than 30.'], 400);
        }
        $data['ownerId'] = $request->header(GetUserFromHeader::USER_HEADER_NAME);

        return Exercise::create($data);
    }

    public function update(Request $request, $id)
    {
        $exercise = Exercise::findOrFail($id);
        if (!$this->isExerciseOwner($exercise, $request)) {
            return Response()->json(['error' => 'No access to manage that exercise.'], 403);
        }

        $data = $request->all();
        if (!$this->isValidInput($data)) {
            return Response()->json(['error' => 'Property minutesToComplete shouldn\'t be bigger than 30.'], 400);
        }
        $data['ownerId'] = $request->header(GetUserFromHeader::USER_HEADER_NAME);

        $exercise->update($data);

        return $exercise;
    }

    public function delete(Request $request, $id)
    {
        $exercise = Exercise::findOrFail($id);
        if (!$this->isExerciseOwner($exercise, $request)) {
            return Response()->json(['error' => 'No access to manage that exercise.'], 403);
        }

        $exercise->delete();

        return 204;
    }

    private function isExerciseOwner(Exercise $exercise, Request $request): bool
    {
        if ($exercise['ownerId'] != $request->header(GetUserFromHeader::USER_HEADER_NAME)) {
            return false;
        }

        return true;
    }

    private function isValidInput(array $data): bool
    {
        if ($data['minutesToComplete'] > 30) {
            return false;
        }

        return true;
    }
}
