<?php

namespace App\Http\Controllers;

use App\Exercise;
use App\Workout;
use App\Http\Middleware\GetUserFromHeader;
use Illuminate\Http\Request;

class WorkoutController extends Controller
{
    public function index()
    {
        return Workout::all();
    }

    public function show($id)
    {
        return Workout::find($id);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $data['ownerId'] = $request->header(GetUserFromHeader::USER_HEADER_NAME);
        unset($data['difficultyLevel']);
        unset($data['minutesToComplete']);

        return Workout::create($data);
    }

    public function update(Request $request, $id)
    {
        $workout = Workout::findOrFail($id);
        if (!$this->isWorkoutOwner($workout, $request)) {
            return Response()->json(['error' => 'No access to manage that workout.'], 403);
        }

        $data = $request->all();
        $data['ownerId'] = $request->header(GetUserFromHeader::USER_HEADER_NAME);
        unset($data['difficultyLevel']);
        unset($data['minutesToComplete']);

        $workout->update($data);

        return $workout;
    }

    public function delete(Request $request, $id)
    {
        $workout = Workout::findOrFail($id);
        if (!$this->isWorkoutOwner($workout, $request)) {
            return Response()->json(['error' => 'No access to manage that workout.'], 403);
        }

        $workout->delete();

        return 204;
    }

    private function isWorkoutOwner(Workout $workout, Request $request): bool
    {
        if ($workout['ownerId'] != $request->header(GetUserFromHeader::USER_HEADER_NAME)) {
            return false;
        }

        return true;
    }

    public function addExerciseIntoWorkout(Request $request, $id, $exerciseId)
    {
        $workout = Workout::findOrFail($id);
        if (!$this->isWorkoutOwner($workout, $request)) {
            return Response()->json(['error' => 'No access to manage that workout.'], 403);
        }
        $exercise = Exercise::findOrFail($exerciseId);

        $workout->exercises()->attach($exercise);

        return $this->recalculateWorkoutData($workout);
    }

    public function removeExerciseFromWorkout(Request $request, $id, $exerciseId)
    {
        $workout = Workout::findOrFail($id);
        if (!$this->isWorkoutOwner($workout, $request)) {
            return Response()->json(['error' => 'No access to manage that workout.'], 403);
        }
        $exercise = Exercise::findOrFail($exerciseId);

        $workout->exercises()->detach($exercise);

        return $this->recalculateWorkoutData($workout);
    }

    private function recalculateWorkoutData(Workout $workout): Workout
    {
        $amount = $sumDifficultyLevel = $sumMinutesToComplete = 0;
        foreach ($workout->exercises()->get() as $exercise) {
            $amount++;
            $sumDifficultyLevel += $exercise['difficultyLevel'];
            $sumMinutesToComplete += $exercise['minutesToComplete'];
        }

        $workout->update([
            'difficultyLevel' => $amount ? round($sumDifficultyLevel/$amount) : 0,
            'minutesToComplete' => $sumMinutesToComplete
        ]);

        return $workout;
    }
}
