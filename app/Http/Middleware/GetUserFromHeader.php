<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\TrimStrings as Middleware;

class GetUserFromHeader extends Middleware
{
    public const USER_HEADER_NAME = 'X-User-Id';

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     * @throws \Exception
     */
    public function handle($request, $next)
    {
        if (!$request->hasHeader(self::USER_HEADER_NAME)) {
            return Response()->json([
                'error' => sprintf(
                    'User identify header "%s" missing.',
                    self::USER_HEADER_NAME
                )
            ], 403);
        }

        return $next($request);
    }
}
