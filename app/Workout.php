<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workout extends Model
{
    protected $fillable = ['title', 'difficultyLevel', 'minutesToComplete', 'isPopular', 'ownerId'];

    public $timestamps = false;

    public function exercises()
    {
        return $this->belongsToMany(
            'App\Exercise',
            'workoutExercise',
            'workoutId',
            'exerciseId'
        );
    }
}
