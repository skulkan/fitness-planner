<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ExercisesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('exercises')->delete(1);
        DB::table('exercises')->updateOrInsert([
            'id' => 1,
            'title' => 'exercise 1',
            'difficultyLevel' => 3,
            'minutesToComplete' => 15,
            'ownerId' => 1,
        ]);

        DB::table('exercises')->delete(2);
        DB::table('exercises')->updateOrInsert([
            'id' => 2,
            'title' => 'exercise 2',
            'difficultyLevel' => 1,
            'minutesToComplete' => 20,
            'ownerId' => 1,
        ]);

        DB::table('exercises')->delete(3);
        DB::table('exercises')->updateOrInsert([
            'id' => 3,
            'title' => 'exercise 3',
            'difficultyLevel' => 2,
            'minutesToComplete' => 20,
            'ownerId' => 1,
        ]);

        DB::table('exercises')->delete(4);
        DB::table('exercises')->updateOrInsert([
            'id' => 4,
            'title' => 'exercise 4',
            'difficultyLevel' => 4,
            'minutesToComplete' => 12,
            'ownerId' => 1,
        ]);

        DB::table('exercises')->delete(5);
        DB::table('exercises')->updateOrInsert([
            'id' => 5,
            'title' => 'exercise 5',
            'difficultyLevel' => 1,
            'minutesToComplete' => 22,
            'ownerId' => 1,
        ]);
    }
}
