<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete(1);
        DB::table('users')->insert([
            'id' => 1,
            'name' => 'test1',
            'email' => 'test1@example.com',
            'password' => bcrypt('password'),
        ]);

        DB::table('users')->delete(2);
        DB::table('users')->insert([
            'id' => 2,
            'name' => 'test2',
            'email' => 'test2@example.com',
            'password' => bcrypt('password'),
        ]);

        DB::table('users')->delete(3);
        DB::table('users')->insert([
            'id' => 3,
            'name' => 'test3',
            'email' => 'test3@example.com',
            'password' => bcrypt('password'),
        ]);
    }
}
