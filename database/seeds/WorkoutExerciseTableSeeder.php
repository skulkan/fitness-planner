<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WorkoutExerciseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('workoutExercise')->truncate();
        DB::table('workoutExercise')->insert([
            'workoutId' => 1,
            'exerciseId' => 1,
        ]);

    }
}
