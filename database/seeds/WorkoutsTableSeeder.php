<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WorkoutsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('workouts')->delete(1);
        DB::table('workouts')->insert([
            'id' => 1,
            'title' => 'workout 1',
            'difficultyLevel' => 3,
            'minutesToComplete' => 15,
            'isPopular' => true,
            'ownerId' => 1,
        ]);

        DB::table('workouts')->delete(2);
        DB::table('workouts')->insert([
            'id' => 2,
            'title' => 'workout 2',
            'difficultyLevel' => 0,
            'minutesToComplete' => 0,
            'isPopular' => false,
            'ownerId' => 1,
        ]);

        DB::table('workouts')->delete(3);
        DB::table('workouts')->insert([
            'id' => 3,
            'title' => 'workout 3',
            'difficultyLevel' => 0,
            'minutesToComplete' => 0,
            'isPopular' => false,
            'ownerId' => 1,
        ]);

        DB::table('workouts')->delete(4);
        DB::table('workouts')->insert([
            'id' => 4,
            'title' => 'workout 4',
            'difficultyLevel' => 0,
            'minutesToComplete' => 0,
            'isPopular' => false,
            'ownerId' => 1,
        ]);

        DB::table('workouts')->delete(5);
        DB::table('workouts')->insert([
            'id' => 5,
            'title' => 'workout 5',
            'difficultyLevel' => 0,
            'minutesToComplete' => 0,
            'isPopular' => false,
            'ownerId' => 1,
        ]);
    }
}
