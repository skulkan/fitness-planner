
## How to run

- Checkout that repo
- composer install
- Set up .env from example using existing database credentials
- php artisan migrate
- php artisan db:seed
- php artisan serve

## User Stories and background

### Description
In the context of a 'Fitness Planner' platform REST api, we would like you to implement the following user
stories:
### User story 1
As an api consumer, I should be able to CRUD an exercise.
Conditions of satisfaction:
An exercise contains the following:
a title
a difficulty level
the minutes to complete
Any user can create an exercise
Any user can show/read any exercise
Only the creator/owner of an exercise can manage it (update, delete)
An exercise cannot take longer than 30 minutes to complete

### User story 2
As an api consumer, I should be able to CRUD a workout.
Conditions of satisfaction:
An workout contains:
a title
the total minutes to complete the workout
a difficulty level
an indication if the workout is popular
Any user can create an workout
Any user can show/read any workout
Only the creator/owner of an workout can manage it (update, delete)

###User story 3
As an api consumer, I should be able to add/remove exercises to a workout.
Conditions of satisfaction:
A workout consists of 0 or more exercises

Only the creator/owner of the workout can add/remove exercises
Any exercise can be added to a workout regardless of which user created/owns the exercise
The total minutes to complete the workout will be the sum of minutes of all exercises contained
within
The difficulty level of the workout is calculated as the average of difficulty levels of all exercises
contained within

###User story 4
As an api consumer, I should be able to recommend a workout
Conditions of satisfaction:
A user cannot recommend their own workouts, only other workouts created/owned by other
users
When a workout is recommended, the workout owner and all users who created/own exercises in
the workout should get a notification by email
A user can only recommend a workout once ... not more
A user is not allowed to recommend more than 2 workouts per day
If a workout gets 3 recommendations, then it is flagged as a popular workout

###User story 5
As an api consumer, I should be able to get a listing of all available workouts
Conditions of satisfaction:
All users have full visiblity over all workouts including the related exercises
