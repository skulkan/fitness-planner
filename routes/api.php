<?php

use App\Http\Middleware\GetUserFromHeader;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('exercises', 'ExerciseController@index');
Route::get('exercises/{id}', 'ExerciseController@show');

Route::get('workouts', 'WorkoutController@index');
Route::get('workouts/{id}', 'WorkoutController@show');

Route::group(['middleware' => [GetUserFromHeader::class]], function () {
    Route::post('exercises', 'ExerciseController@store');
    Route::put('exercises/{id}', 'ExerciseController@update');
    Route::delete('exercises/{id}', 'ExerciseController@delete');

    Route::post('workouts', 'WorkoutController@store');
    Route::put('workouts/{id}', 'WorkoutController@update');
    Route::delete('workouts/{id}', 'WorkoutController@delete');

    Route::post('workouts/{id}/exercises/{exerciseId}', 'WorkoutController@addExerciseIntoWorkout');
    Route::delete('workouts/{id}/exercises/{exerciseId}', 'WorkoutController@removeExerciseFromWorkout');
});

