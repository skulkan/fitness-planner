<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AttachDeattachExerciseToWorkoutTest extends TestCase
{
    use DatabaseTransactions;

    private const TEST_USER_ID = 1;
    private const TEST_WRONG_USER_ID = 3;

    public function testAttach()
    {
        $response = $this->post(
            '/api/workouts/1/exercises/2',
            [],
            [
                'X-User-Id' => self::TEST_USER_ID
            ]
        );

        $response->assertStatus(200);

        $body = $response->decodeResponseJson();

        $this->assertEquals('workout 1', $body['title']);
        $this->assertEquals(2, $body['difficultyLevel']);
        $this->assertEquals(35, $body['minutesToComplete']);
        $this->assertEquals(self::TEST_USER_ID, $body['ownerId']);
    }

    public function testDetach()
    {
        $response = $this->delete(
            '/api/workouts/1/exercises/1',
            [],
            [
                'X-User-Id' => self::TEST_USER_ID
            ]
        );

        $response->assertStatus(200);

        $body = $response->decodeResponseJson();

        $this->assertEquals('workout 1', $body['title']);
        $this->assertEquals(0, $body['difficultyLevel']);
        $this->assertEquals(0, $body['minutesToComplete']);
        $this->assertEquals(self::TEST_USER_ID, $body['ownerId']);
    }

    public function testManyAttachDetach()
    {
        $response = $this->post(
            '/api/workouts/1/exercises/2',
            [],
            [
                'X-User-Id' => self::TEST_USER_ID
            ]
        );
        $response->assertStatus(200);
        $body = $response->decodeResponseJson();
        $this->assertEquals(2, $body['difficultyLevel']);
        $this->assertEquals(35, $body['minutesToComplete']);

        $response = $this->post(
            '/api/workouts/1/exercises/3',
            [],
            [
                'X-User-Id' => self::TEST_USER_ID
            ]
        );
        $response->assertStatus(200);
        $body = $response->decodeResponseJson();
        $this->assertEquals(2, $body['difficultyLevel']);
        $this->assertEquals(55, $body['minutesToComplete']);

        $response = $this->post(
            '/api/workouts/1/exercises/4',
            [],
            [
                'X-User-Id' => self::TEST_USER_ID
            ]
        );
        $response->assertStatus(200);
        $body = $response->decodeResponseJson();
        $this->assertEquals(3, $body['difficultyLevel']);
        $this->assertEquals(67, $body['minutesToComplete']);

        $response = $this->post(
            '/api/workouts/1/exercises/5',
            [],
            [
                'X-User-Id' => self::TEST_USER_ID
            ]
        );
        $response->assertStatus(200);
        $body = $response->decodeResponseJson();
        $this->assertEquals(2, $body['difficultyLevel']);
        $this->assertEquals(89, $body['minutesToComplete']);

        $response = $this->delete(
            '/api/workouts/1/exercises/5',
            [],
            [
                'X-User-Id' => self::TEST_USER_ID
            ]
        );
        $response->assertStatus(200);
        $body = $response->decodeResponseJson();
        $this->assertEquals(3, $body['difficultyLevel']);
        $this->assertEquals(67, $body['minutesToComplete']);

        $response = $this->delete(
            '/api/workouts/1/exercises/2',
            [],
            [
                'X-User-Id' => self::TEST_USER_ID
            ]
        );
        $response->assertStatus(200);
        $body = $response->decodeResponseJson();
        $this->assertEquals(3, $body['difficultyLevel']);
        $this->assertEquals(47, $body['minutesToComplete']);

        $response = $this->delete(
            '/api/workouts/1/exercises/3',
            [],
            [
                'X-User-Id' => self::TEST_USER_ID
            ]
        );
        $response->assertStatus(200);
        $body = $response->decodeResponseJson();
        $this->assertEquals(4, $body['difficultyLevel']);
        $this->assertEquals(27, $body['minutesToComplete']);

        $response = $this->delete(
            '/api/workouts/1/exercises/1',
            [],
            [
                'X-User-Id' => self::TEST_USER_ID
            ]
        );
        $response->assertStatus(200);
        $body = $response->decodeResponseJson();
        $this->assertEquals(4, $body['difficultyLevel']);
        $this->assertEquals(12, $body['minutesToComplete']);

        $response = $this->delete(
            '/api/workouts/1/exercises/4',
            [],
            [
                'X-User-Id' => self::TEST_USER_ID
            ]
        );
        $response->assertStatus(200);
        $body = $response->decodeResponseJson();
        $this->assertEquals(0, $body['difficultyLevel']);
        $this->assertEquals(0, $body['minutesToComplete']);
    }

    public function testAttachWithWrongUserHeader()
    {
        $response = $this->post(
            '/api/workouts/1/exercises/2',
            [],
            [
                'X-User-Id' => self::TEST_WRONG_USER_ID
            ]
        );

        $response->assertStatus(403);
    }

    public function testDetachWithWrongUserHeader()
    {
        $response = $this->delete(
            '/api/workouts/1/exercises/1',
            [],
            [
                'X-User-Id' => self::TEST_WRONG_USER_ID
            ]
        );

        $response->assertStatus(403);
    }
}
