<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class DeleteOneExerciseTest extends TestCase
{
    use DatabaseTransactions;

    private const TEST_USER_ID = 1;
    private const TEST_WRONG_USER_ID = 3;

    public function testDelete()
    {
        $response = $this->delete(
            '/api/exercises/1',
            [],
            [
                'X-User-Id' => self::TEST_USER_ID
            ]
        );

        $response->assertStatus(200);

        $body = $response->decodeResponseJson();
    }

    public function testDeleteWithoutUserHeader()
    {
        $response = $this->delete(
            '/api/exercises/1',
            [],
            []
        );

        $response->assertStatus(403);
    }

    public function testDeleteWithWrongUserHeader()
    {
        $response = $this->delete(
            '/api/exercises/1',
            [],
            [
                'X-User-Id' => self::TEST_WRONG_USER_ID
            ]
        );

        $response->assertStatus(403);
    }
}
