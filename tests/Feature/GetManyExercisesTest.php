<?php

namespace Tests\Feature;

use Tests\TestCase;

class GetManyExercisesTest extends TestCase
{
    public function testGet()
    {
        $response = $this->get('/api/exercises');

        $response->assertStatus(200);

        $body = $response->decodeResponseJson();

        $this->assertEquals('exercise 1', $body[0]['title']);
        $this->assertEquals(3, $body[0]['difficultyLevel']);
        $this->assertEquals(15, $body[0]['minutesToComplete']);
        $this->assertEquals(1, $body[0]['ownerId']);

        $this->assertEquals('exercise 5', $body[4]['title']);
        $this->assertEquals(1, $body[4]['difficultyLevel']);
        $this->assertEquals(22, $body[4]['minutesToComplete']);
        $this->assertEquals(1, $body[4]['ownerId']);
    }
}
