<?php

namespace Tests\Feature;

use Tests\TestCase;

class GetOneExerciseTest extends TestCase
{
    /**
     * @dataProvider getProvider
     */
    public function testGet($id, $title, $difficultyLevel, $minutesToComplete, $ownerId)
    {
        $response = $this->get('/api/exercises/' . $id);

        $response->assertStatus(200);

        $body = $response->decodeResponseJson();

        $this->assertEquals($title, $body['title']);
        $this->assertEquals($difficultyLevel, $body['difficultyLevel']);
        $this->assertEquals($minutesToComplete, $body['minutesToComplete']);
        $this->assertEquals($ownerId, $body['ownerId']);
    }

    public function getProvider()
    {
        return [
            [1, 'exercise 1', 3, 15, 1],
            [3, 'exercise 3', 2, 20, 1],
            [2, 'exercise 2', 1, 20, 1],
            [4, 'exercise 4', 4, 12, 1],
            [5, 'exercise 5', 1, 22, 1],
        ];
    }
}
