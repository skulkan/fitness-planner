<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class PostOneExerciseTest extends TestCase
{
    use DatabaseTransactions;

    private const TEST_USER_ID = 2;

    public function testPost()
    {
        $response = $this->post(
            '/api/exercises',
            [
                'title' => 'test',
                'difficultyLevel' => 1,
                'minutesToComplete' => 5
            ],
            [
                'X-User-Id' => self::TEST_USER_ID
            ]
        );

        $response->assertStatus(201);

        $body = $response->decodeResponseJson();

        $this->assertEquals('test', $body['title']);
        $this->assertEquals(1, $body['difficultyLevel']);
        $this->assertEquals(5, $body['minutesToComplete']);
        $this->assertEquals(self::TEST_USER_ID, $body['ownerId']);
    }

    public function testPostWithToManyMinutes()
    {
        $response = $this->post(
            '/api/exercises',
            [
                'title' => 'test',
                'difficultyLevel' => 1,
                'minutesToComplete' => 31
            ],
            [
                'X-User-Id' => self::TEST_USER_ID
            ]
        );

        $response->assertStatus(400);
    }

    public function testPostWithoutUserHeader()
    {
        $response = $this->post(
            '/api/exercises',
            [
                'title' => 'test',
                'difficultyLevel' => 1,
                'minutesToComplete' => 5
            ],
            []
        );

        $response->assertStatus(403);
    }
}
