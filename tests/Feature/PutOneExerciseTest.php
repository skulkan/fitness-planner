<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class PutOneExerciseTest extends TestCase
{
    use DatabaseTransactions;

    private const TEST_USER_ID = 1;
    private const TEST_WRONG_USER_ID = 3;

    public function testPut()
    {
        $response = $this->put(
            '/api/exercises/1',
            [
                'title' => 'test',
                'difficultyLevel' => 1,
                'minutesToComplete' => 5
            ],
            [
                'X-User-Id' => self::TEST_USER_ID
            ]
        );

        $response->assertStatus(200);

        $body = $response->decodeResponseJson();

        $this->assertEquals('test', $body['title']);
        $this->assertEquals(1, $body['difficultyLevel']);
        $this->assertEquals(5, $body['minutesToComplete']);
        $this->assertEquals(self::TEST_USER_ID, $body['ownerId']);
    }

    public function testPutWithToMAnyMinutes()
    {
        $response = $this->put(
            '/api/exercises/1',
            [
                'title' => 'test',
                'difficultyLevel' => 1,
                'minutesToComplete' => 31
            ],
            [
                'X-User-Id' => self::TEST_USER_ID
            ]
        );

        $response->assertStatus(400);
    }

    public function testPutWithoutUserHeader()
    {
        $response = $this->put(
            '/api/exercises/1',
            [
                'title' => 'test',
                'difficultyLevel' => 1,
                'minutesToComplete' => 5
            ],
            []
        );

        $response->assertStatus(403);
    }

    public function testPutWithWrongUserHeader()
    {
        $response = $this->put(
            '/api/exercises/1',
            [
                'title' => 'test',
                'difficultyLevel' => 1,
                'minutesToComplete' => 5
            ],
            [
                'X-User-Id' => self::TEST_WRONG_USER_ID
            ]
        );

        $response->assertStatus(403);
    }
}
